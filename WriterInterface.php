<?php
namespace Iss\Api\Service\RequestRecorder;

interface WriterInterface
{
    public function write($key, array $data, bool $close = false);
}