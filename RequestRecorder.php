<?php
namespace Iss\Api\Service\RequestRecorder;

use Iss\Api\Service\RequestRecorder\Writer\{Blank, Redis};
use Iss\Api\ServiceInterface;
use Iss\Api\ServiceTrait;
use Phalcon\Config\Config;
use Phalcon\Mvc\Micro;
use Phalcon\Events\Event;

class RequestRecorder implements ServiceInterface
{
    use ServiceTrait;

    protected array $_data;

    protected Config $_config;

    protected WriterInterface $_writer;

    public function __construct()
    {
        $this->_data = [];
        $this->setPriority(50);
    }

    public function register(Micro $application) : bool
    {
        if (!$application->getDI()->has('config')) {
            // config service is not ready yet
            return false;
        }
        $this->_config = $application['config'](
            'service/'.self::getName(), __DIR__.'/config'
        )->merge(
            $application['config']('service/'.self::getName())
        );
        if ($this->_config->path('redis.enabled', false)) {
            unset($this->_config->redis->enabled);
            $this->_writer = new Redis($this->_config->redis);
        } else {
            $this->_writer = new Blank();
        }
        $application->eventsManager->attach('api', $this, $this->getPriority());
        $application->getDI()->setShared(self::getName(), $this);
        return true;
    }

    public function unregister(Micro $application) : ?ServiceInterface
    {
        $application->getEventsManager()->detach('api', $this);
        $application->getDI()->remove(self::getName());
        return $this;
    }

    public static function getName() : string
    {
        return 'requestrecorder';
    }

    public function beforeHandleRequest(Event $event, Micro $application)
    {
        $request = $application['request'];

        switch ($request->getMethod()) {
            case "POST":
            case "PUT":
            case "PATCH":
            case "DELETE":
                break ;
            default:
                return true;
        }

        $this->_data['header'] = $request->getHeaders();
        $this->_data['uuid'] = $application['uuid'];
        $this->_data['uri'] = $request->getUri();
        $this->_data['parameters'] = $request->get();
        $this->_data['payload'] = json_decode($request->getRawBody()) ?? $request->getRawBody();
        $this->_data['method'] = $request->getMethod();

        try {
            $this->_writer->write($this->_data['uuid'], $this->_data, false);
        } catch (\Exception $e) {
            $application['logger']->warning('[%service%] ' . $e->getMessage());
        }
        return true;
    }

    public function afterHandleRequest(Event $event, Micro $application)
    {
        switch ($application['request']->getMethod()) {
            case "POST":
            case "PUT":
            case "PATCH":
            case "DELETE":
                break ;
            default:
                return true;
        }

        $response = $application['response'];
        $this->_data['status_code'] = $response->getHeaders()->get('Status');

        $principal = $application['authentication']->getPrincipal();
        if ($principal) {
            $this->_data['principal'] = ['name' => $principal['name'], 'ucy_id' => $principal['ucy_id']];
        } else {
            $this->_data['principal'] = ['name' => 'Unauthorized', 'ucy_id' => '0000000000000'];
        }
        $this->_data['on'] = time();

        try {
            $this->_writer->write($this->_data['uuid'], $this->_data, true);
        } catch (\Exception $e) {
            $application['logger']->warning('[%service%] ' . $e->getMessage());
        }

        return true;
    }
}