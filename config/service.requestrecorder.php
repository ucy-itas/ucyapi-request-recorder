<?php

/**
 * Configuration settings. Defaults will be overridden from config
 *
 * array['redis']
 *          ['host']
 *          ['port']
 *          ['db']
 *          ['key']
 *          ['enabled']
 */
return [
    'redis' => [
        'key' => 'submitted-requests',
        'enabled' => false,
        'timeout' => 2,
        'db' => 1
    ]
];