<?php
namespace Iss\Api\Service\RequestRecorder\Writer;

use Iss\Api\Service\RequestRecorder\WriterInterface;
use Phalcon\Config\Config;

class Redis implements WriterInterface
{
    protected \Redis $_client;

    protected Config $_config;

    protected bool $_connected = false;

    public function __construct(Config $config)
    {
        $this->_config = $config;
        $this->_client = new \Redis();
    }

    public function write($key, array $data, bool $close = false)
    {
        if ($this->connect()) {
            $this->_client->hSet($this->_config->key, $key, json_encode($data));
            if ($close) {
                $this->disconnect();
            }
        }
    }

    protected function connect()
    {
        if (!$this->_connected) {
            $timeout = $this->_config->timeout ?? 60;
            $this->_connected = $this->_client->connect($this->_config->host, $this->_config->port, $timeout);
            $this->_client->select($this->_config->get('db', 0));
        }
        return $this->_connected;
    }

    protected function disconnect()
    {
        if ($this->_connected) {
            $this->_client->close();
            $this->_connected = false;
        }
        return $this->_connected;
    }
}