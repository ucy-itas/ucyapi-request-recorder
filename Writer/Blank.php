<?php
namespace Iss\Api\Service\RequestRecorder\Writer;

use Iss\Api\Service\RequestRecorder\WriterInterface;

class Blank implements WriterInterface
{
    public function write($key, array $data, bool $close = false)
    {
        return;
    }
}